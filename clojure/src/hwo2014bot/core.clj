(ns hwo2014bot.core
  (:require [clojure.data.json :as json])
  (:use [aleph.tcp :only [tcp-client]]
        [lamina.core :only [enqueue wait-for-result wait-for-message]]
        [gloss.core :only [string]]
        [hwo2014bot.logic])
  (:gen-class))

(defn- json->clj [string]
  (json/read-str string :key-fn keyword))

(defn send-message [channel message]
  (enqueue channel (json/write-str message)))

(defn read-message [channel]
  (json->clj
      (wait-for-message channel)))

(defn connect-client-channel [host port]
  (wait-for-result
   (tcp-client {:host host,
                :port port,
                :frame (string :utf-8 :delimiters ["\n"])})))

(defn log-msg [msg]
  ;(println msg)
  (case (:msgType msg)
    "gameInit" (println msg)
    "join" (println "Joined")
    "gameStart" (println "Race started")
    "crash" (println "Someone crashed")
    "gameEnd" (println msg)
    ;"carPositions" (println msg)
    "error" (println (str "ERR: " (:data msg)))
    :noop))

(defn game-loop [channel]
  (.set turbo 1)
  (.set turbo-ticks 0)
  (loop [channel channel]
    (if-let [msg (try (read-message channel)
                   (catch Exception e
                     (println (str "ERROR: " (.getMessage e)))))]
      (do
        (log-msg msg)
        (if-let [result (handle-msg msg)]
          (send-message channel result))
        (recur channel)))))

(defn launch-race [host port botname botkey track]
  (let [channel (connect-client-channel host (Integer/parseInt port))]
    (send-message channel {:msgType "createRace", :data {:botId {:name botname, :key botkey}, :trackName track, :password "secret-password", :carCount 1}})
    (game-loop channel)))

(defn -main [& [host port botname botkey]]
   (let [channel (connect-client-channel host (Integer/parseInt port))]
     (send-message channel {:msgType "join" :data {:name botname :key botkey}})
     (game-loop channel)
     (System/exit 0)))

(defn multi-launch[]
  (pmap #(launch-race
          "testserver.helloworldopen.com"
          "8091"
          "V24Code"
          "pCB57N5epK0t7Q"
          %)
        ["keimola" "germany" "usa"]))

(defn run-times [n track]
  (if (= n 0)
    nil
    (do
      (launch-race "testserver.helloworldopen.com" "8091" "V24Code" "pCB57N5epK0t7Q" track)
      (recur (dec n) track))))
