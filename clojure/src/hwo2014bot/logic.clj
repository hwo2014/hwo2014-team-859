(ns hwo2014bot.logic)

(def my-car (ThreadLocal.))
(def track (ThreadLocal.))
(def prev-status (ThreadLocal.))
(def max-speeds (ThreadLocal.))
(def prev-speed (ThreadLocal.))
(def turbo (ThreadLocal.))
(def turbo-ticks (ThreadLocal.))
(def run-turbo (ThreadLocal.))

(defn acceleration []
  (* 0.2 (.get turbo)))

(defn speed [me prev]
  (let [position (get-in me [:piecePosition :inPieceDistance])
        previous (get-in prev [:piecePosition :inPieceDistance])]
    (if previous
      (- position previous)
      0)))

(defn log-status [& coll]
  (println coll))

(defn piece-angle [index]
  (get-in (.get track) [:pieces index :angle] 0))

(defn abs-piece-angle [index]
  (Math/abs (piece-angle index)))

(defn piece-radius
  ([index]
   (piece-radius index (.get track)))
  ([index track]
   (get-in track [:pieces index :radius] 1000)))

(defn length [index]
  (let [piece (get-in (.get track) [:pieces index])
        length (:length piece)]
    (if (nil? length)
      (* (abs-piece-angle index) (:radius piece) (/ Math/PI 180))
      length)))

(defn next-piece
  ([index] (next-piece index (.get track)))
  ([index track]
   (let [next-index (inc index)]
     (if (<= (count (:pieces track)) next-index)
       0
       next-index))))

(defn previous-piece
  ([index] (previous-piece index (.get track)))
  ([index track]
   (let [previous-index (dec index)
         last-index (dec (count (:pieces track)))]
     (if (< previous-index 0)
       last-index
       previous-index))))

(defn lerp [x0 x1 a]
  (+ x0 (* (- x1 x0) a)))

;; TODO
;;  - Parametrize these settings
;;
; Sub-optimal results in keimola:
;(<= radius 75)
;(if (> in-piece-travel-ratio 0.7) 4.65 4.65)
;(<= radius 125)
;(if (> in-piece-travel-ratio 0.4) 6.5 6.5)
;;

;;
; Best results in germany:
;(<= radius 75)
;(if (> in-piece-travel-ratio 0.7) 5.1 4.25)
;(<= radius 125)
;(if (> in-piece-travel-ratio 0.4) 7.1 6.25)
;;

(comment (defn max-speed
  ([piece] (max-speed piece 0))
  ([piece in-piece-distance]
   (let [radius (piece-radius piece)
         in-piece-travel-ratio (/ in-piece-distance (length piece))]
     (cond
      (<= radius 75)
      (if (> in-piece-travel-ratio 0.7) 5.1 4.25)
      (<= radius 125)
      (if (> in-piece-travel-ratio 0.4) 7.1 6.25)
      :else
      10)))))

(defn piece-index [piece track]
  (.indexOf (:pieces track) piece))

(defn def-max-speed [piece track]
  (let [radius (piece-radius piece track)]
    (cond
     (< radius 50)
     3.5
     (< radius 100)
     4.5
     (< radius 150)
     5.5
     (< radius 200)
     6.5
     (< radius 250)
     7.5
     (< radius 300)
     8.5
     (< radius 350)
     9.5
     :else
     30)))

(comment (defn def-max-speed [index track]
  (let [ppiece (previous-piece index track)
        helper (fn [index times]
                 (let [npiece (next-piece index track)
                       radius (piece-radius npiece track)]
                   (cond
                    (< (piece-radius index) radius)
                    times
                    :else
                    (recur npiece (inc times)))))]
    (cond
     (< 300 (piece-radius index track))
     (def-max-speed-max index track)
     (< (piece-radius index track) (piece-radius ppiece track))
     (- (def-max-speed-max index track) (* (helper index 0) 0.08))
     :else
     (- (def-max-speed ppiece track) (* (helper index 0) 0.08))))))

(defn initial-max-speeds [track]
  (let [pieces (:pieces track)]
    (into [] (cons 0 (map #(def-max-speed % track) (range (count pieces)))))))


(comment (def test-track {:id "usa", :name "USA", :pieces [{:length 100.0} {:length 100.0, :switch true} {:radius 200, :angle 22.5} {:radius 200, :angle 22.5} {:radius 200, :angle 22.5} {:radius 200, :angle 22.5} {:length 100.0} {:length 100.0, :switch true} {:radius 200, :angle 22.5} {:radius 200, :angle 22.5} {:radius 200, :angle 22.5} {:radius 200, :angle 22.5} {:length 100.0} {:length 100.0} {:length 100.0} {:length 100.0} {:length 100.0} {:length 100.0, :switch true} {:radius 200, :angle 22.5} {:radius 200, :angle 22.5} {:radius 200, :angle 22.5} {:radius 200, :angle 22.5} {:length 100.0} {:length 100.0, :switch true} {:radius 200, :angle 22.5} {:radius 200, :angle 22.5} {:radius 200, :angle 22.5} {:radius 200, :angle 22.5} {:length 100.0} {:length 100.0} {:length 100.0} {:length 100.0}], :lanes [{:distanceFromCenter -20, :index 0} {:distanceFromCenter 0, :index 1} {:distanceFromCenter 20, :index 2}], :startingPoint {:position {:x -340.0, :y -96.0}, :angle 90.0}})
(.set track test-track)
(:pieces test-track)
(piece-radius 1 test-track)
(def-max-speed 5 test-track)
(def speeds (initial-max-speeds test-track))
(assoc speeds 0 (inc (first speeds)))
(new-speeds speeds 0 0))

(defn new-speeds [speeds piece modifier]
  (let [speed (get speeds (inc piece))
        prev (previous-piece piece)
        pspeed (get speeds (inc prev))
        helper (fn [speeds piece modifier depth]
                 (cond
                  (<= depth 0)
                  speeds
                  :else
                  (recur (assoc speeds (inc piece) (+ modifier (get speeds (inc piece))))
                         (previous-piece piece) (/ modifier 2) (dec depth))))]
    (if (< modifier 0)
      ;(assoc (assoc speeds piece (+ speed modifier)) prev (+ pspeed (/ modifier 2)))
      (helper speeds piece (* modifier 8) 6)
      (assoc speeds (inc piece) (+ speed modifier)))))

(defn update-max-speeds [me speed]
  (let [angle (Math/abs (:angle me))
        piece (get-in me [:piecePosition :pieceIndex])
        ppiece (previous-piece piece)
        speeds (if (== piece 0)
                 (assoc (.get max-speeds) 0 (inc (first (.get max-speeds))))
                 (.get max-speeds))
        modifier (/ (- 0.55 (/ angle 100)) (first speeds))]
    (log-status piece angle modifier speeds)
    (cond
     (and (> modifier 0) (< (+ 0.1 speed) (get speeds (inc piece))))
     (.set max-speeds speeds)
     (> angle 99)
     (.set max-speeds ;Work around bug..
           (new-speeds (assoc speeds (inc piece) (- (get speeds (inc piece)) 0.04))
                      ppiece -0.02))
     :else
     (.set max-speeds (new-speeds speeds piece modifier)))))

(defn max-speed
  ([piece] (max-speed piece 0))
  ([piece in-piece-distance]
   (let [radius (piece-radius piece)
         ratio (/ in-piece-distance (length piece))
         speed (get (.get max-speeds) (inc piece))]
       speed)))

(defn max-acceleration [speed]
  (let [acceleration (acceleration)
        drag 0.98]
    (- (+ acceleration (* speed drag)) speed)))

(defn decceleration [speed]
  (let [step 0.02]
    (* speed step)))

(defn throttle [speed target-speed angle]
  (let [accel (acceleration)
        deccel (decceleration speed)]
    (cond
     (> speed target-speed)
     (let [diff (- speed target-speed)]
       (if (> diff deccel)
         0
         (/ (- deccel diff) accel)))
     :else
     (let [diff (- target-speed (- speed deccel))
           throttle (/ diff accel)]
       (if (> throttle 1)
         1
         throttle)))))

(defn travel-to-target [speed target-speed]
  (let [step 0.98
        acceleration (acceleration)
        slowdown (fn [speed ticks travel]
                   (let [trav (+ speed travel)]
                     (cond
                      (> ticks 150)
                      [trav ticks]
                      (<= speed target-speed)
                      [trav ticks]
                      :else
                      (recur (* speed step) (inc ticks) trav))))
        accelerate (fn [start ticks travel]
                     (cond
                      (> ticks 150)
                      [travel ticks]
                      (>= start target-speed)
                      [travel ticks]
                      :else
                      (recur (+ (* start step) acceleration)
                             (inc ticks)
                             (+ travel start))))]
    (cond
     (> speed target-speed)
     (slowdown speed 0 speed)
     :else
     (accelerate speed 0 speed))))

(defn target-speed [speed piece position depth]
  (let [helper (fn [npiece distance depth]
                 ;(log-status speed (max-speed npiece) (first (travel-to-target speed (max-speed npiece))) distance depth)
                 (cond
                   (= depth 0)
                    (max-speed piece position)
                   (and (< (max-speed npiece) (+ 0.1 speed))
                        (> (first (travel-to-target speed (max-speed npiece))) distance))
                    (max-speed npiece)
                   :else
                    (recur (next-piece npiece)
                           (+ distance (length npiece))
                           (dec depth))))]
    (helper (next-piece piece) (- (length piece) position) depth)))

(defn switch-lane [piece]
  (let [angle (piece-angle (next-piece piece))]
    (if (< 0 angle)
      "Left"
      "Right")))

(defmulti handle-msg :msgType)

(defmethod handle-msg "crash" [msg]
  (if (= (.get my-car) (:data msg))
    (update-max-speeds (assoc (.get prev-status) :angle 100) 100)))

(defmethod handle-msg "turboAvailable" [msg]
  (let [data (:data msg)
        factor (:turboFactor data)
        ticks (:turboDurationTicks data)]
    (log-status "Turbo" factor ticks)
    (.set turbo factor)
    (.set turbo-ticks ticks)
    (.set run-turbo true)))

(defmethod handle-msg "carPositions" [msg]
  (let [tick (:gameTick msg)
        prev (.get prev-status)
        me (first (filter #(= (.get my-car) (:id %)) (:data msg)))
        angle (Math/abs (:angle me))
        speed (speed me prev)
        pspeed (.get prev-speed)
        piece (get-in me [:piecePosition :pieceIndex])
        position (get-in me [:piecePosition :inPieceDistance])
        target (target-speed speed piece position 5)]
    ;(println (str tick \, speed \, angle \, (max-speed piece position) \, target \, piece))
    ;(log-status (travel-to-target speed target) speed (max-speed piece) target piece)
    (.set prev-status me)
    (.set prev-speed speed)
    ;(log-status ((acceleration) (throttle speed target angle)))
    (if (<= (.get turbo-ticks) 0)
      (do
        (.set turbo 1))
      (.set turbo-ticks (dec (.get turbo-ticks))))
    (cond
     (< speed 0)
     (do
       (update-max-speeds prev pspeed)
       (if (.get run-turbo)
         (do (.set run-turbo false) {:msgType "turbo", :data "Moar speed", :gameTick tick})
         {:msgType "switchLane", :data (switch-lane piece), :gameTick tick}))
     :else
     {:msgType "throttle" :data (throttle speed target angle), :gameTick tick})))

(defmethod handle-msg "yourCar" [msg]
  (.set my-car (:data msg))
  {:msgType "ping" :data "ping"})

(defn file-path [file]
  (clojure.java.io/resource file))

(defn read-max-speeds []
  (let [track (.get track)]
    (try
      (get (read-string (slurp "data")) (:id track) (initial-max-speeds track))
      (catch Exception e
        (try
          (get (read-string (slurp (file-path "data"))) (:id track) (initial-max-speeds track))
          (catch Exception e
            (println "Read failed, using fallback values")
            (initial-max-speeds track)))))))

(defn save-current-track []
  (let [cur (try (into {} (read-string (slurp "data")))
              (catch Exception e
                (try
                  (into {} (read-string (slurp (file-path "data"))))
                  (catch Exception e
                    {}))))
        track (.get track)
        speeds (.get max-speeds)]
    (try
      (spit (file-path "data") (pr-str (assoc cur (:id track) speeds)))
      (catch Exception e
        (try
          (spit "data" (pr-str (assoc cur (:id track) speeds)))
          (catch Exception e
            (println "Write failed")))))))

(defmethod handle-msg "gameInit" [msg]
  (let [new-track (get-in msg [:data :race :track])]
    (.set track new-track)
    (.set max-speeds (read-max-speeds))
    {:msgType "ping" :data "ping"}))

(defmethod handle-msg "gameEnd" [msg]
  (save-current-track)
  {:msgType "ping" :data "ping"})

(defmethod handle-msg :default [msg]
  {:msgType "ping" :data "ping"})
